package com.icms.common.basic;

public class AdminController extends BaseController {

	protected void renderAdmin(String template) {
		render("/" + ADMIN_TEMPLATE_PATH + "/" + template + "." + DEFAULT_HTML_SUFFIX);
	}

	protected void renderRefreshIframe(String status, String info,String url) {
		setAttr("status", status);
		setAttr("url", url);
		setAttr("info", info);
		render("/common/status" + "." + DEFAULT_HTML_SUFFIX);
		return;
	}

	
}
