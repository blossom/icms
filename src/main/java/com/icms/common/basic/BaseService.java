package com.icms.common.basic;

import org.apache.log4j.Logger;

import com.icms.common.util.DbUtil;

public abstract class BaseService {

	protected final Logger log = Logger.getLogger(getClass());
	protected DbUtil dao = DbUtil.getInstance();
	protected abstract void clearCacheAll();
}
