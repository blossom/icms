package com.icms.common.basic;

/**
 * @author jin E-mail： jin@westboy.net
 * @version 创建时间：2016年9月22日 下午7:35:03
 */
public class MemberController extends BaseController {
	
	protected void renderDefaultIndex(String template) {
		render("/" + FRONT_PATH + "/" + DEFAULT_TEMPLATE_PATH + "/" + INDEX + "/" + template + "."
				+ DEFAULT_HTML_SUFFIX);
	}

	protected void renderDefaultCategory(String template) {
		render("/" + FRONT_PATH + "/" + DEFAULT_TEMPLATE_PATH + "/" + CATEGORY + "/" + template + "."
				+ DEFAULT_HTML_SUFFIX);
	}

	protected void renderDefaultContent(String template) {
		render("/" + FRONT_PATH + "/" + DEFAULT_TEMPLATE_PATH + "/" + CONTENT + "/" + template + "."
				+ DEFAULT_HTML_SUFFIX);
	}

	protected void renderDefaultAlone(String template) {
		render("/" + FRONT_PATH + "/" + DEFAULT_TEMPLATE_PATH + "/" + ALONE + "/" + template + "."
				+ DEFAULT_HTML_SUFFIX);
	}

	protected void renderDefaultList(String template) {
		render("/" + FRONT_PATH + "/" + DEFAULT_TEMPLATE_PATH + "/" + LIST + "/" + template + "."
				+ DEFAULT_HTML_SUFFIX);
	}
}
