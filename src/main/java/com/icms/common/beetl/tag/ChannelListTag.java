package com.icms.common.beetl.tag;

import java.util.List;

import org.beetl.core.GeneralVarTagBinding;

import com.icms.common.config.AppConfig;
import com.icms.common.vo.ChannelVo;
import com.icms.core.model.Category;
import com.icms.core.model.Website;
import com.icms.core.service.CategoryService;
import com.icms.core.service.SiteService;

public class ChannelListTag extends GeneralVarTagBinding {

	@Override
	public void render() {
		
		int siteId = Integer.parseInt((String) this.getAttributeValue("siteId"));
		Long catId = Long.parseLong((String) this.getAttributeValue("parentId"));
//		Long hasContent = Long.parseLong((String) this.getAttributeValue("hasContent"));
		int isNav = Integer.parseInt((String) this.getAttributeValue("isNav"));
		Website site = SiteService.service.getWebsiteBySiteId(siteId);
		List<Category> categorys = CategoryService.service.getCategoryByParentId(catId,isNav);
		ChannelVo channel = new ChannelVo();
		
		if(categorys!=null&&!categorys.isEmpty()){
			
			for(Category category :categorys){
				String url = AppConfig.HttpProtocol+"://"+site.getDomain()+"/"+category.getShortname()+AppConfig.suffix;	
				channel.setUrl(url);
				channel.setName(category.getCategoryName());
				this.binds(channel);
				this.doBodyRender();
			}	
			
		}else{
			
			Category category =CategoryService.service.getCategoryByCatId(catId);
			String url = AppConfig.HttpProtocol+"://"+site.getDomain()+"/"+category.getShortname()+AppConfig.suffix;
			channel.setUrl(url);
			channel.setName(category.getCategoryName());
			this.binds(channel);
			this.doBodyRender();
			
		}
	}

}
