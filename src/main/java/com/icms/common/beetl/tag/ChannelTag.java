package com.icms.common.beetl.tag;

import org.beetl.core.GeneralVarTagBinding;

import com.icms.common.config.AppConfig;
import com.icms.common.vo.ChannelVo;
import com.icms.core.model.Category;
import com.icms.core.model.Website;
import com.icms.core.service.CategoryService;
import com.icms.core.service.SiteService;

public class ChannelTag extends GeneralVarTagBinding{

	@Override
	public void render() {
		int siteId = Integer.parseInt((String) this.getAttributeValue("siteId"));
		int isMore = Integer.parseInt((String) this.getAttributeValue("isMore"));
		Long catId = Long.parseLong((String) this.getAttributeValue("channelId"));
		ChannelVo channel = new ChannelVo();
		Website site = SiteService.service.getWebsiteBySiteId(siteId);
		Category category = CategoryService.service.getCategoryByCatId(catId);
		String url = AppConfig.HttpProtocol+"://"+site.getDomain()+"/"+category.getShortname()+AppConfig.suffix;
		if(isMore==1){
			url=AppConfig.HttpProtocol+"://"+site.getDomain()+"/"+category.getShortname()+"/-list"+AppConfig.suffix;
			category.setCategoryName("更多");
		}
		channel.setUrl(url);
		channel.setName(category.getCategoryName());
		this.binds(channel);
		this.doBodyRender();
		
	}

}
