package com.icms.common.beetl.tag;

import org.beetl.core.GeneralVarTagBinding;

import com.icms.common.config.AppConfig;
import com.icms.core.model.Category;
import com.icms.core.model.Content;
import com.icms.core.model.Website;
import com.icms.core.service.CategoryService;
import com.icms.core.service.ContentService;
import com.icms.core.service.SiteService;

public class ContentTag extends GeneralVarTagBinding {

	@Override
	public void render() {
		int siteId = Integer.parseInt((String) this.getAttributeValue("siteId"));
		Long catId =  (Long)this.getAttributeValue("channelId");
		Long contentId = (Long)this.getAttributeValue("contentId");
		int titleLen = Integer.parseInt((String) this.getAttributeValue("titleLen"));
		// 0为下一篇，1为上一篇
		int nextId = Integer.parseInt((String) this.getAttributeValue("next"));
		//contentId必须大于0
		if(contentId > 0 && nextId == 1 ){
			contentId--;
		}else{
			contentId++;
		}
		Website site = SiteService.service.getWebsiteBySiteId(siteId);
		Category category = CategoryService.service.getCategoryByCatId(catId);
		Content content = ContentService.service.getContentByContentId(contentId);
		if(content!=null){
			if(content.getTitle().length()>titleLen){
				content.setTitle(content.getTitle().substring(0,titleLen)+"...");
			}
			String url = AppConfig.HttpProtocol+"://"+site.getDomain()+"/"+category.getShortname()+"/content-"+content.getContentId()+AppConfig.suffix;
			content.setUrl(url);
		}
		this.binds(content);
		this.doBodyRender();
	}

}
