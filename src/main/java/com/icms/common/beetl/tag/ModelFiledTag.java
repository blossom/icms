package com.icms.common.beetl.tag;

import java.util.Map;
import java.util.Map.Entry;

import org.beetl.core.GeneralVarTagBinding;

import com.icms.common.config.AppConfig;
import com.icms.common.util.StrUtil;
import com.icms.common.vo.ModelFiledVo;
import com.icms.core.model.ModelFiled;
import com.icms.core.model.Website;
import com.icms.core.service.ModelFiledService;
import com.icms.core.service.SiteService;

public class ModelFiledTag extends GeneralVarTagBinding {

	@SuppressWarnings("unchecked")
	@Override
	public void render() {
		int siteId = Integer.parseInt((String) this.getAttributeValue("siteId"));
		int modelId = Integer.parseInt((String) this.getAttributeValue("modelId"));
		Long catId = Long.parseLong((String) this.getAttributeValue("channelId"));
		String filedName = (String) this.getAttributeValue("filedName");
		Map<String, Object> param = (Map<String, Object>) this.getAttributeValue("param");
		Website site = SiteService.service.getWebsiteBySiteId(siteId);
		ModelFiled modelFiled = ModelFiledService.service.getModelFiledByFiledName(filedName);
		String url = AppConfig.HttpProtocol + "://" + site.getDomain() + "/search?m=" + modelId + "&c=" + catId + "&"
				+ filedName + "={filedvalue}";
		String[] values = modelFiled.getFiledValue().split("#");
		String str = "";
		String kv = "";
		boolean flag = true;
		if ((StrUtil.StrEquals(modelFiled.getFiledClass(), "radio"))) {
			for (String value : values) {
				ModelFiledVo mfv = new ModelFiledVo();
				if (param != null) {
					if (!param.isEmpty()) {
						for (Entry<String, Object> entry : param.entrySet()) {
							if (!StrUtil.StrEquals(filedName, entry.getKey())) {
								if (!StrUtil.StrEquals(kv, entry.getKey() + entry.getValue())) {
									str += "&" + entry.getKey() + "=" + entry.getValue();
								}
								kv = entry.getKey() + entry.getValue();
							}
						}
					}
				}
				if (flag) {
					mfv.setUrl(AppConfig.HttpProtocol + "://" + site.getDomain() + "/search?m=" + modelId + "&c="
							+ catId + str);
					mfv.setName("全部");
					this.binds(mfv);
					this.doBodyRender();
					flag=false;
				}
				mfv.setUrl(url.replace("{filedvalue}", value) + str);
				mfv.setName(value);
				this.binds(mfv);
				this.doBodyRender();

			}
		}
	}

	public static void main(String[] args) {
		String l = "http://127.0.0.1:8080/search?m=48&c=5&test=&test=qwe";
		System.out.println(l.contains("test"));
	}

}
