package com.icms.common.config;

import com.icms.common.beetl.BeetlRenderFactory;
import com.icms.common.ext.FakeStaticHandler;
import com.icms.common.ext.RenderingTimeHandler;
import com.icms.common.interceptor.SystemInterceptor;
import com.icms.common.shiro.ShiroInterceptor;
import com.icms.common.shiro.ShiroPlugin;
import com.icms.core.model._MappingKit;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.interceptor.SessionInViewInterceptor;
import com.jfinal.kit.LogKit;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.plugin.redis.Redis;
import com.jfinal.plugin.redis.RedisPlugin;

import net.sf.ehcache.Cache;

public class AppConfig extends JFinalConfig {

	private Routes routes;
	// 系统后台路径
	private String AdminPath;
	// 系统伪静态后缀
	public static String suffix;
	// 主机http协议
	public static String HttpProtocol;

	@Override
	public void configConstant(Constants me) {
		LogKit.debug(">>>>>>>>>>>>>>> 系统常量初始化");
		PropKit.use("application.properties");
		AdminPath = PropKit.get("system.AdminPath");
		AppConfig.suffix = PropKit.get("system.rewrite");
		AppConfig.HttpProtocol = PropKit.get("system.HttpProtocol");
		me.setEncoding("utf-8");
		me.setUrlParaSeparator("-");
		me.setMainRenderFactory(new BeetlRenderFactory());
		me.setDevMode(PropKit.getBoolean("system.dev", false));
		me.setMaxPostSize(1024 * 1024);
		me.setError404View("/common/404.html");
		me.setError500View("/common/500.html");
	}

	@Override
	public void configRoute(Routes me) {
		LogKit.info(">>>>>>>>>>>>>>> 加载路由");
		this.routes = me;
		me.add(new SystemRoutes(AdminPath));
		me.add(new HomeRoutes());
	}

	@Override
	public void configPlugin(Plugins me) {
		LogKit.info(">>>>>>>>>>>>>>> 加载shiro插件");
		ShiroPlugin shiroPlugin = new ShiroPlugin(routes);
		shiroPlugin.setLoginUrl(AdminPath + "/login");
		shiroPlugin.setSuccessUrl(AdminPath);
		shiroPlugin.setUnauthorizedUrl("/401");
		me.add(shiroPlugin);
		LogKit.info(">>>>>>>>>>>>>>> 加载EhcachePlugin");
		me.add(new EhCachePlugin());
		LogKit.info(">>>>>>>>>>>>>>> 加载DruidPlugin");
		DruidPlugin druidPlugin = new DruidPlugin(PropKit.get("jdbc.url"), PropKit.get("jdbc.user"),
				PropKit.get("jdbc.password"));
		me.add(druidPlugin);
		// 配置ActiveRecord插件
		ActiveRecordPlugin arp = new ActiveRecordPlugin(druidPlugin);
		arp.setShowSql(true);
		me.add(arp);
		_MappingKit.mapping(arp);
//		RedisPlugin bbsRedis = new RedisPlugin("bbs", "localhost");
//		com.jfinal.plugin.redis.Cache bbsCache = Redis.use("bbs");
//		bbsCache.
//		me.add(bbsRedis);

	}

	@Override
	public void configInterceptor(Interceptors me) {
		me.add(new ShiroInterceptor());
		LogKit.info(">>>>>>>>>>>>>>> 加载shiro拦截器");
		me.add(new SystemInterceptor(AdminPath));
		LogKit.info(">>>>>>>>>>>>>>> 加载系统后台拦截器");
		me.add(new SessionInViewInterceptor());

	}

	@Override
	public void configHandler(Handlers me) {
		LogKit.info(">>>>>>>>>>>>>>> 加载控制器");
		me.add(new RenderingTimeHandler());
		me.add(new FakeStaticHandler());

	}

	public static void main(String[] args) {

		JFinal.start("src/main/webapp", 8080, "/", 5);

	}

}
