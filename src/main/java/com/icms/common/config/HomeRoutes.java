package com.icms.common.config;

import com.icms.web.index.IndexController;
import com.jfinal.config.Routes;

public class HomeRoutes extends Routes {

	@Override
	public void config() {
		add("/", IndexController.class);
	}

}
