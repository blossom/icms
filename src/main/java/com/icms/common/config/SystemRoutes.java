package com.icms.common.config;

import com.icms.web.admin.CategoryController;
import com.icms.web.admin.ContentController;
import com.icms.web.admin.ModelController;
import com.icms.web.admin.ModelFiledController;
import com.icms.web.admin.SystemController;
import com.jfinal.config.Routes;
import com.jfinal.core.Controller;

public class SystemRoutes extends Routes {

	private String systemPath = null;

	public SystemRoutes(String path) {
		this.systemPath = path;
	}

	@Override
	public void config() {
		add("/", SystemController.class);
		add("/model", ModelController.class);
		add("/model/filed", ModelFiledController.class);
		add("/category", CategoryController.class);
		add("/content", ContentController.class);
	}

	@Override
	public Routes add(String controllerKey, Class<? extends Controller> controllerClass) {
		controllerKey = controllerKey.equals("/") ? systemPath : systemPath + controllerKey;
		return super.add(controllerKey, controllerClass);
	}
}
