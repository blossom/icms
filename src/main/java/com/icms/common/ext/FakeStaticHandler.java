package com.icms.common.ext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.icms.common.config.AppConfig;
import com.jfinal.handler.Handler;

public class FakeStaticHandler extends Handler {
	
	public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
		int index = target.lastIndexOf(AppConfig.suffix);
		if (index != -1) {
			target = target.substring(0, index);
		}
		next.handle(target, request, response, isHandled);
	}
}
