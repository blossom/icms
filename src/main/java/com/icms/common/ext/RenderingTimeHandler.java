package com.icms.common.ext;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.icms.common.beetl.BeetlRender;
import com.icms.common.beetl.BeetlRenderFactory;
import com.icms.common.config.AppConfig;
import com.icms.common.util.ServletUtil;
import com.jfinal.handler.Handler;

public class RenderingTimeHandler extends Handler {

    private final Logger logger = Logger.getLogger(getClass());

    @Override
    public void handle(String target, HttpServletRequest request, HttpServletResponse response, boolean[] isHandled) {
    	logger.debug("###############RenderingTimeHandler###############");
    	BeetlRender.startTime=System.currentTimeMillis();
        BeetlRenderFactory.siteVar.put("basePath", AppConfig.HttpProtocol+"://"+ServletUtil.getDomain(request));
        BeetlRenderFactory.siteVar.put("res", AppConfig.HttpProtocol+"://"+ServletUtil.getDomain(request)+"/r");
        next.handle(target, request, response, isHandled);
        long endTime = System.currentTimeMillis();
        logger.debug("processTime:"+ (endTime - BeetlRender.startTime) + "ms");
      
    }



}

