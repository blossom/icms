package com.icms.common.util;

import java.io.*;  
import org.apache.commons.httpclient.*;  
import org.apache.commons.httpclient.methods.*;  
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.icms.common.config.AppConfig;
import com.jfinal.kit.PathKit;  
  
/** 静态页面引擎 */  
public class HtmlGeneratorUtil {  
	
	private final static Logger logger = LoggerFactory.getLogger(HtmlGeneratorUtil.class);
	
    private HttpClient httpClient = null; //HttpClient实例  
    private GetMethod getMethod =null; //GetMethod实例  
    private BufferedWriter fw = null;  
    private String page = null;  
    private BufferedReader br = null;  
    private InputStream in = null;  
    private StringBuffer sb = null;  
    private String line = null;  
    private String domainPath = null;  
    private int siteId = 0;
    private final String staticPath = "/html/"; 
    private String webPath = null;
    private String writePath =  null;
    private  String viewExtension = ".html";
    
	public HtmlGeneratorUtil(String domain,int Id) {
		this.domainPath= domain;
		this.siteId = Id;
		webPath = domainPath +staticPath+siteId;
		writePath = PathKit.getWebRootPath() +staticPath.replace("/", "\\")+siteId+"\\";
	}

	/** 根据模版及参数产生静态页面 */  
    public boolean createHtmlPage(String url,String htmlFileName){  
    	logger.debug("##################生成静态的站点Id:"+siteId+" #######################");
        boolean status = false;   
        url =AppConfig.HttpProtocol+"://"+ url;
        int statusCode = 0;         
        htmlFileName =  writePath+htmlFileName+viewExtension;
        File f= new File(writePath);
        if(!f.isDirectory()){
        	f.mkdir();
        }
        try{  
            //创建一个HttpClient实例充当模拟浏览器  
            httpClient = new HttpClient();  
            //设置httpclient读取内容时使用的字符集  
            httpClient.getParams().setParameter(HttpMethodParams.HTTP_CONTENT_CHARSET,"UTF-8");           
            //创建GET方法的实例  
            getMethod = new GetMethod(url);  
            //使用系统提供的默认的恢复策略，在发生异常时候将自动重试3次  
            getMethod.getParams().setParameter(HttpMethodParams.RETRY_HANDLER, new DefaultHttpMethodRetryHandler());  
            //设置Get方法提交参数时使用的字符集,以支持中文参数的正常传递  
            getMethod.addRequestHeader("Content-Type","text/html;charset=UTF-8");  
            //执行Get方法并取得返回状态码，200表示正常，其它代码为异常  
            statusCode = httpClient.executeMethod(getMethod);   
            logger.info("静态页面引擎在解析"+url+"产生静态页面"+htmlFileName);  
            if (statusCode!=200) {  
            	logger.info("静态页面引擎在解析"+url+"产生静态页面"+htmlFileName+"时出错!");  
            }else{  
                //读取解析结果  
                sb = new StringBuffer();  
                in = getMethod.getResponseBodyAsStream();  
                br = new BufferedReader(new InputStreamReader(in,"UTF-8"));  
                while((line=br.readLine())!=null){  
                    sb.append(line+"\n");  
                }  
                if(br!=null)br.close();  
                page = sb.toString();  
                //将页面中的相对路径替换成绝对路径，以确保页面资源正常访问  
                page = formatPage(page);  
                //将解析结果写入指定的静态HTML文件中，实现静态HTML生成  
                writeHtml(htmlFileName,page);  
                status = true;  
            }             
        }catch(Exception ex){  
            logger.info("静态页面引擎在解析"+url+"产生静态页面"+htmlFileName+"时出错:"+ex.getMessage());           
        }finally{  
            //释放http连接  
            getMethod.releaseConnection();  
        }  
        return status;  
    }  
      
    //将解析结果写入指定的静态HTML文件中  
    private synchronized void writeHtml(String htmlFileName,String content) throws Exception{  
        fw = new BufferedWriter(new FileWriter(htmlFileName));  
        fw.write(page);   
        if(fw!=null)fw.close();       
    }  
      
    //将页面中的相对路径替换成绝对路径，以确保页面资源正常访问  
    private String formatPage(String page){       
        page = page.replaceAll("basePath", webPath);  
        return page;  
    }  
  
}  