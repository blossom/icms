package com.icms.core.model;

import com.icms.core.model.base.BaseMemberGroup;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
public class MemberGroup extends BaseMemberGroup<MemberGroup> {
	public static final MemberGroup dao = new MemberGroup();
}
