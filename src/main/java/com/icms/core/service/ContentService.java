package com.icms.core.service;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.icms.common.basic.BaseService;
import com.icms.common.config.AppConst;
import com.icms.core.model.Content;
import com.jfinal.aop.Enhancer;
import com.jfinal.kit.LogKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.ehcache.CacheKit;

public class ContentService extends BaseService {

	public static final ContentService service = Enhancer.enhance(ContentService.class);
	private final String table = " ic_content ";
	private final String cacheName = AppConst.SERVICE_CACHE[AppConst.CONTENT_CACHE];

	public Page<Content> getContentPageByCatId(int pageNumber, long catid) {
		String select = "select * ";
		String sqlExceptSelect = "from " + table + " where category_id = ? ";
		return Content.dao.paginateByCache(cacheName, "content_page_" + pageNumber + catid, pageNumber,
				AppConst.PAGE_SIZE, select, sqlExceptSelect, catid);
	}

	public Page<Content> getContentAllPage(int pageNumber) {
		String select = "select * ";
		String sqlExceptSelect = "from " + table;
		return Content.dao.paginateByCache(cacheName, "content_all_page_" + pageNumber, pageNumber, AppConst.PAGE_SIZE,
				select, sqlExceptSelect);
	}

	public Page<Record> getContentByModelIdAndFiledNames(int pageNumber,String tableName,int modelId,Map<String,Object> keys) {
		String where= "1=1";
		String selectFiled="";
		String key="";
		for(Entry<String, Object> entry:keys.entrySet()){ 
			where += " and "+entry.getKey()+" = "+"'"+entry.getValue()+"'";
			selectFiled+="n."+entry.getKey()+",";
			key+=entry.getValue();
	    } 
		String select = dao.select(selectFiled+"c.*").exec();
		String sqlExceptSelect = dao
				.empty()
				.from("ic_content_"+tableName+" n")
				.join("ic_content c ")
				.on("n.content_id = c.content_id")
				.where(where).exec();
		LogKit.info("生成查询语句："+select+" - - - -- - - "+selectFiled);
		LogKit.info("生成查询语句："+select+" - - - -- - - "+sqlExceptSelect);
		return Db.paginateByCache(cacheName, "content_ModelIdAndFiledNames_page_" + pageNumber+key, pageNumber, AppConst.PAGE_SIZE,
		select, sqlExceptSelect);
	}

	public List<Content> getContentListByCatId(long catId) {
		return Content.dao.findByCache(cacheName, "content_list_" + catId,
				"select * from " + table + " where catid = ? ", catId);
	}

	public Content getContentByContentId(Long contentId) {

		return Content.dao.findFirstByCache(cacheName, "content_contentId_" + contentId,
				"select * from " + table + "where content_id = ?", contentId);

	}

	/**
	 * 查询内容列表
	 * 
	 * @param SiteId
	 * @param catId
	 * @return
	 */
	public List<Content> getContentListBySiteIdAndCatId(long SiteId, long catId, int recommend, int orderBy) {

		// String sql ="select * "
		// + " from "
		// +" ic_category as cat inner join ic_content as c "
		// +" on cat.category_id = c.category_id "
		// +" where "
		// +" site_id = ? "
		// +" and "
		// +" cat.category_id = ? "
		// +" and "
		// +" c.recommend = ? "
		// +" order by c.content_id ";
		String sql = dao.select("*")
				.from("ic_category cat")
				.join(" ic_content  c ")
				.on("cat.category_id = c.category_id")
				.where("site_id = ?")
				.and("cat.category_id = ?")
				.and("c.recommend = ?")
				.orderBy("c.content_id", 1)
				.exec();
		log.info(sql);
		return Content.dao.findByCache(cacheName, "content_list_" + catId + SiteId, sql, SiteId, catId, recommend);
	}

	/***
	 * 查询内容列表
	 * 
	 * @param SiteId
	 * @param catId
	 * @return
	 */
	public List<Content> getContentListBySiteIdAndParentId(long SiteId, long catId, int recommend, int orderBy) {

		String sql = "select * " + "from ic_category as cat inner join ic_content as c "
				+ "on cat.category_id = c.category_id " + "where " + "site_id = ? " + "and " + "cat.parent_id = ? "
				+ " or " + "cat.category_id = ? " + "order by content_id ";
		if (orderBy == 1) {
			sql += " desc ";
		}
		return Content.dao.findByCache(cacheName, "content_list_" + catId + SiteId, sql, SiteId, catId, catId,
				recommend);
	}

	@Override
	protected void clearCacheAll() {
		CacheKit.removeAll(cacheName);
	}

}
