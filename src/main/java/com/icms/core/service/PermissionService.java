package com.icms.core.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.icms.common.basic.BaseService;
import com.icms.common.config.AppConst;
import com.icms.core.model.Permission;
import com.jfinal.aop.Enhancer;
import com.jfinal.plugin.ehcache.CacheKit;

public class PermissionService extends BaseService {
	
	public static final PermissionService service = Enhancer.enhance(PermissionService.class);
	private final String cacheName = AppConst.SERVICE_CACHE[7];
	
	public Set<String> getPermissionsByUsername(String username) {
		String sql = "select p.* "
				+ "from ic_admin a, ic_role r, ic_permission p,ic_admin_role ar, ic_role_permission rp "
				+ "where a.admin_id = ar.admin_id and r.role_id = ar.role_id and r.role_id = rp.role_id and p.permission_id = rp.permisson_id and a.username = ?";
		List<Permission> permissions = Permission.dao.findByCache(cacheName, "permission" + username, sql, username);
		Set<String> set = new HashSet<String>();
		for (Permission p : permissions) {
			set.add(p.getStr("name"));
		}
		return set;
	}
	@Override
	protected void clearCacheAll() {
		CacheKit.removeAll(cacheName);	
	}
}
