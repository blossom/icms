package com.icms.core.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.icms.common.basic.BaseService;
import com.icms.common.config.AppConst;
import com.icms.core.model.Role;
import com.jfinal.aop.Enhancer;
import com.jfinal.plugin.ehcache.CacheKit;

public class RoleService extends BaseService {

	public static final RoleService service = Enhancer.enhance(RoleService.class);
	private final String cacheName = AppConst.SERVICE_CACHE[6];
	
	public Set<String> getRolesByUsername(String username) {

		String sql = "select r.* "
				+ "from ic_admin a, ic_role r, ic_admin_role ar "
				+ "where a.admin_id= ar.admin_id and r.role_id = ar.role_id and a.username  = ?";
		Set<String> set = new HashSet<String>();
		List<Role> roles = Role.dao.findByCache(cacheName, "roles"+username, sql, username);
		for (Role r : roles) {
			set.add(r.getStr("name"));
		}
		return set;
	}
	
	@Override
	protected void clearCacheAll() {
		CacheKit.removeAll(cacheName);	
	}
}
