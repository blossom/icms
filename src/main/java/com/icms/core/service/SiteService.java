package com.icms.core.service;

import com.icms.common.basic.BaseService;
import com.icms.common.config.AppConst;
import com.icms.core.model.Website;
import com.jfinal.aop.Enhancer;
import com.jfinal.plugin.ehcache.CacheKit;

public class SiteService extends BaseService {

	public static final SiteService service = Enhancer.enhance(SiteService.class);
	public static final String table = "ic_website";
	public static final String cacheName = AppConst.SERVICE_CACHE[AppConst.SITE_CACHE];

	public Website getWebsiteByDomain(String domain) {
		String sql = "select * from " + table + " where domain=?";
		return Website.dao.findFirstByCache(cacheName, "website_domain" + domain, sql, domain);
	}
	
	public Website getWebsiteBySiteId(int siteId) {
		String sql = "select * from " + table + " where site_id=?";
		return Website.dao.findFirstByCache(cacheName, "website_" + siteId, sql, siteId);
	}
	
	@Override
	protected void clearCacheAll() {
		CacheKit.removeAll(cacheName);	
	}
}
