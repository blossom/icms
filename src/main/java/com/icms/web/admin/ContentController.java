package com.icms.web.admin;

import java.util.List;

import com.icms.common.basic.AdminController;
import com.icms.core.model.Category;
import com.icms.core.model.Content;
import com.icms.core.model.Model;
import com.icms.core.model.ModelFiled;
import com.icms.core.service.CategoryService;
import com.icms.core.service.ContentService;
import com.icms.core.service.ModelFiledService;
import com.icms.core.service.ModelService;
import com.jfinal.plugin.activerecord.Page;

public class ContentController extends AdminController {

	public void index() {
		renderAdmin("content-main");
	}

	public void list() {
		long catId = getParaToLong(0);
		int pageNumber = getParaToInt(1, 1);
		Page<Content> contentPage=null;
		Category  category=null;
		String view = "content-list";
		if (catId > 0){ 
			category = CategoryService.service.getCategoryByCatId(catId);
			if(category.getAlone()){
				view = "category-content";
				setAttr("content", category.getContent());
			}else{
			 contentPage = ContentService.service.getContentPageByCatId(pageNumber, catId);
			 setAttr("contentPage", contentPage);
			}
		}else{
			contentPage = ContentService.service.getContentAllPage(pageNumber);
			setAttr("contentPage", contentPage);
		}
		setAttr("catId", catId);
		renderAdmin(view);
	}

	public void input() {
		long catId = getParaToLong(0);
		Category category = CategoryService.service.getCategoryByCatId(catId);
		Model model = ModelService.service.getModelByModelId(category.getModelId());
		List<ModelFiled> modelFileds = ModelFiledService.service.getModelFiledByModelId(model.getModelId());
        setAttr("modelName", model.getTableName());  
        setAttr("modelFileds", modelFileds);  
        renderAdmin("content-input");
	}

	public void edit(){
		
	}
	
}
