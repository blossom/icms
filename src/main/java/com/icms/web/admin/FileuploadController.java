package com.icms.web.admin;

import java.util.List;

import com.icms.common.basic.BaseController;
import com.icms.common.util.UploadUtil;
import com.jfinal.upload.UploadFile;

public class FileuploadController extends BaseController {

	public void index(){
		render("/test.html");
	}
	
	public void upload(){
		List<UploadFile> files = getFiles();
		renderText(UploadUtil.uploadFile(files,"",null).toString());
	}
}
