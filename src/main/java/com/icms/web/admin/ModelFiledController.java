package com.icms.web.admin;

import java.util.List;

import com.icms.common.basic.AdminController;
import com.icms.common.util.DbTableUtil;
import com.icms.core.model.ModelFiled;
import com.icms.core.service.ModelFiledService;
import com.icms.core.service.ModelService;

public class ModelFiledController extends AdminController{

	public void index() {
		int modelId = getParaToInt(0);
		List<ModelFiled> list = ModelFiledService.service.getModelFiledByModelId(modelId);
		setAttr("modelId", modelId);
		setAttr("list", list);
		renderAdmin("modelfiled-list");

	}

	public void input() {
		Integer modelId = getParaToInt(0);
		Integer modelFiledId = getParaToInt(1);
		if (modelFiledId != null) {
			ModelFiled modelFiled = ModelFiledService.service.getModelFiledByFiledId(modelFiledId);
			setAttr("modelFiled", modelFiled);
		}
		setAttr("modelId", modelId);
		renderAdmin("modelFiled-input");

	}

	public void save() {

		ModelFiled modelFiled = getModel(ModelFiled.class);
		if (ModelFiledService.service.createModelFiled(modelFiled)) {
			renderSuccess("操作成功");
		} else {
			renderError("操作失败");
		}
	}

	public void del() {
		Integer modelFiledId = getParaToInt(0);
		Integer modelId = getParaToInt(1);
		String tableName = ModelService.service.getModelByModelId(modelId).getTableName();
		String filedName = ModelFiledService.service.getModelFiledByFiledId(modelFiledId).getFiledName();
		if (ModelFiledService.service.deleteByModelFiledId(modelFiledId)) {
			DbTableUtil.delTableFiled(tableName, filedName);
			renderJson(true);
		} else {
			renderJson(true);
		}
	}

	public void validateFiledname() {
		String filedName = getPara("modelFiled.filed_name");
		if (ModelFiledService.service.getModelFiledByFiledName(filedName) == null) {
			renderJson(true);
		} else {
			renderJson(false);
		}
	}
}
