package com.icms.web.admin;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.apache.shiro.authz.annotation.RequiresPermissions;

import com.icms.common.basic.AdminController;
import com.icms.common.config.AppConst;
import com.icms.common.ext.CacheInterceptor;
import com.icms.common.shiro.ShiroUtil;
import com.icms.core.model.Admin;
import com.icms.core.model.Category;
import com.icms.core.service.AdminService;
import com.icms.core.service.CategoryService;
import com.jfinal.aop.Before;
import com.jfinal.core.ActionKey;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.ehcache.CacheName;
import com.jfinal.render.CaptchaRender;
import com.jfinal.render.JsonRender;

public class SystemController extends AdminController {

	@RequiresPermissions("admin:add")
	public void index() {
		Integer siteId = (Integer) ShiroUtil.getSessionAttr(AppConst.SITE_ID);
		List<Category> list = CategoryService.service.getCategoryListBySiteId(siteId);
		setAttr("catlist", list);
		renderAdmin("index");
	}

	public void login() {
		if (isGet()) {
			renderAdmin(LOGIN);
		} else {
			String username = getPara("username");
			String password = getPara("password");
			Boolean remberme = getPara("remberme") == null ? false : true;
			Map<String, Object> result = AdminService.service.login(username, password, remberme);
			render(new JsonRender(result).forIE());
		}
	}

	@ActionKey(value = "/verify/captchaImg")
	public void verifyImage() {
		renderCaptcha();
	}

	@ActionKey(value = "/verify/validateCaptchaImage")
	public void validateImage() {
		String verifyCode = getPara("verifycode");
		if (CaptchaRender.validate(this, verifyCode)) {
			renderJson(true);
		} else {
			renderJson(false);
		}
	}

	@Before(CacheInterceptor.class)
	@CacheName(AppConst.SYSTEM_CACHE)
	public void welcome() {
		Map<String, Object> system = new HashMap<String, Object>();
		String username = (String)ShiroUtil.getSessionAttr(AppConst.SESSION_ADMIN);
		Admin admin = AdminService.service.getAdminByUsername(username);
		Properties props = System.getProperties(); // 获得系统属性集
		system.put("domain", getDomain());
		system.put("ip", this.getHostIp());
		system.put("host", this.getHost());
		system.put("osName", props.getProperty("os.name"));
		system.put("osArch", props.getProperty("os.arch"));
		system.put("osVersion", props.getProperty("os.version"));
		system.put("javaHome", props.getProperty("java.home"));
		system.put("javaVersion", props.getProperty("java.version"));
		system.put("userName", props.getProperty("user.name"));
		system.put("userHome", props.getProperty("user.home"));
		system.put("userDir", props.getProperty("user.dir"));
		system.put("appDir", PathKit.getWebRootPath());
		system.put("serverDate", new Date());
		setAttr("system", system);
		setAttr("lastIp", admin.getLastip());
		setAttr("loginSum", admin.getLoginsum());
		setAttr("lastDate", admin.getLastdate());
		renderAdmin("welcome");
	}

}
